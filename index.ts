function leftPad0(n: number) {
  if (n === 0) {
    return "00";
  }
  return (n < 10 ? "0" : "") + n;
}

export default function timeSpan(seconds: number) {
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor((seconds % 3600) / 60);
  return [leftPad0(hours), leftPad0(minutes), leftPad0(seconds % 60)].join(":");
}
