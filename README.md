# time-span-format

[![npm](https://img.shields.io/npm/v/time-span-format.svg?style=flat-square)](https://www.npmjs.org/package/time-span-format)
[![The MIT License](https://img.shields.io/badge/license-MIT-orange.svg?style=flat-square)](http://opensource.org/licenses/MIT)
![npm type definitions](https://img.shields.io/npm/types/typescript)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

Convert seconds to "HH:MM:SS".

[Documentation](https://xiechoa06.gitlab.io/time-span-format).

## Quick start

```javascript
import timeSpanFormat from 'time-span-format';

timeSpanFormat(0); // 00:00:00

timeSpanFormat(1 * 3600 + 20 * 60 + 9); // 01:20:09
```

## Installation

```bash
npm i time-span-format
```
