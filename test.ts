import timeSpanFormat from "./index";

test("0", () => {
  expect(timeSpanFormat(0)).toBe("00:00:00");
});

test("basic", () => {
  expect(timeSpanFormat(2 * 3600 + 15 * 60 + 9)).toBe("02:15:09");
});
